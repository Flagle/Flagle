package com.hrabbit.admin.core.common.constant.dictmap;

import com.hrabbit.admin.core.common.constant.dictmap.basemap.AbstractDictMap;

/**
 * 系统角色字典表
 *
 * @Auther: hrabbit
 * @Date: 2018-11-22 1:33 PM
 * @Description:
 */
public class SysMenuDict extends AbstractDictMap {
    @Override
    public void init() {
        put("title", "按钮名称");
        put("code", "按钮编码");
        put("icon", "按钮图标");
        put("parenId", "上级id");
    }

    @Override
    protected void initBeWrapped() {
    }
}
