package com.hrabbit.admin.modual.system.controller;

import com.hrabbit.admin.core.common.annotion.BussinessLog;
import com.hrabbit.admin.core.common.constant.dictmap.SysMenuDict;
import com.hrabbit.admin.core.common.constant.dictmap.SysRoleDict;
import com.hrabbit.admin.core.common.tool.ToolUtil;
import com.hrabbit.admin.core.exception.BaseException;
import com.hrabbit.admin.core.exception.BussinessExceptionEnum;
import com.hrabbit.admin.core.node.MenuNode;
import com.hrabbit.admin.core.response.BaseResponse;
import com.hrabbit.admin.modual.system.bean.SysMenu;
import com.hrabbit.admin.modual.system.bean.SysRole;
import com.hrabbit.admin.modual.system.controller.base.BaseController;
import com.hrabbit.admin.modual.system.service.SysMenuService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * 资源控制层
 *
 * @Auther: hrabbit
 * @Date: 2018-12-30 3:39 PM
 * @Description:
 */
@Controller
@RequestMapping(value = "menu")
public class SysMenuController extends BaseController {

    private static final String BASEURL = "/system/menu";

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 进入到按钮维护主页
     *
     * @return
     */
    @RequestMapping("/")
    public String index() {
        return BASEURL + "/menu_info.html";
    }

    /**
     * 根据id获取按钮
     *
     * @return
     */
    @RequestMapping(value = "getMenuById/{menuId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getMenuInfoById(@PathVariable("menuId") Integer menuId) {
        return sysMenuService.selectById(menuId);
    }

    /**
     * 根据父级id获取按钮
     *
     * @return
     */
    @RequestMapping(value = "getMenuByParentId/{parentId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getTopMenu(@PathVariable("parentId") Integer parentId) {
        List<MenuNode> menuByParentId = sysMenuService.findMenuByParentId(parentId);
        return menuByParentId;
    }

    /**
     * 进入到按钮添加主页
     *
     * @return
     */
    @RequestMapping("/topMenuAdd")
    public String topMenuAdd() {
        return BASEURL + "/menu_top_add.html";
    }

    /**
     * 添加菜单
     *
     * @param sysMenu 菜单
     * @author hrabbit
     * @Date: 2019/1/12 1:44 PM
     */
    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ApiOperation(value = "添加菜单")
    @ApiImplicitParam(value = "SysMenu", name = "SysMenu", dataType = "SysMenu")
    @ResponseBody
    public BaseResponse add(@Valid SysMenu sysMenu, BindingResult result) {
        //验证信息
        if (result.hasErrors()) {
            throw new BaseException(BussinessExceptionEnum.REQUEST_INVALIDATE);
        }
        boolean flag = sysMenuService.addOrUpdate(sysMenu);
        if (flag) {
            SuccessResponse.setErrorEnum(BussinessExceptionEnum.REQUEST_INVALIDATE);
        }
        return SuccessResponse;
    }

    /**
     * 进入到按钮修改主页
     *
     * @return
     */
    @RequestMapping("/topMenuUpdate/{id}")
    public String topMenuUpdate(@PathVariable("id") Integer id, ModelMap modelMap) {
        SysMenu sysMenu = sysMenuService.selectById(id);

        modelMap.addAttribute("menu", sysMenu);
        return BASEURL + "/menu_top_update.html";
    }


    /**
     * 修改按钮信息
     *
     * @return
     */
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @BussinessLog(value = "修改按钮", key = "title", dict = SysMenuDict.class)
    @ResponseBody
    public Object update(SysMenu sysMenu, BindingResult result) {
        //验证信息
        if (result.hasErrors()) {
            throw new BaseException(BussinessExceptionEnum.REQUEST_INVALIDATE);
        }
        boolean flag = sysMenuService.addOrUpdate(sysMenu);
        if (flag) {
            SuccessResponse.setErrorEnum(BussinessExceptionEnum.REQUEST_INVALIDATE);
        }
        return SuccessResponse;
    }


    /**
     * 删除菜单信息
     *
     * @return
     */
    @RequestMapping(value = "delete/{menuId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Object delete(@PathVariable("menuId") Integer menuId) {
        boolean flag = sysMenuService.deleteMenu(menuId);
        if (flag) {
            SuccessResponse.setErrorEnum(BussinessExceptionEnum.SERVER_ERROR);
        }
        return SuccessResponse;
    }


}
