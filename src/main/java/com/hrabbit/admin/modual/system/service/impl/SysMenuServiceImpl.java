package com.hrabbit.admin.modual.system.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hrabbit.admin.core.common.tool.ToolUtil;
import com.hrabbit.admin.core.node.MenuNode;
import com.hrabbit.admin.core.shiro.bean.ShiroUser;
import com.hrabbit.admin.core.shiro.util.ShiroUtils;
import com.hrabbit.admin.modual.system.bean.SysMenu;
import com.hrabbit.admin.modual.system.mapper.SysMenuMapper;
import com.hrabbit.admin.modual.system.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.tools.Tool;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 资源业务层
 *
 * @Auther: hrabbit
 * @Date: 2018-12-30 3:40 PM
 * @Description:
 */
@Slf4j
@Service
@SuppressWarnings("all")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    /**
     * 获取角色ids下的资源信息
     *
     * @param roleIds
     * @return
     */
    @Override
    public List<MenuNode> findPermissionByRoleIds(List<Integer> roleIds) {
        //获取到资源
        List<MenuNode> permissionByRoleIds = sysMenuMapper.findPermissionByRoleIds(roleIds);
        //组装资源
        permissionByRoleIds = MenuNode.buildTitle(permissionByRoleIds);
        log.info("资源=>{}", permissionByRoleIds);
        return permissionByRoleIds;
    }


    /**
     * 根据parentId获取按钮信息
     *
     * @param parentId
     * @return
     */
    @Override
    public List<MenuNode> findMenuByParentId(Integer parentId) {
        if (ToolUtil.isNotEmpty(parentId) && parentId != -1) {
            List<MenuNode> data = new ArrayList<>();
            List<MenuNode> menuNodes = this.findMenuNodeByParentId(parentId);
            MenuNode menuNodeId = sysMenuMapper.findMenuNodeId(parentId);
            data.add(menuNodeId);
            data.addAll(menuNodes);
            data = MenuNode.buildTitle(data);
            return data;
        } else {
            return sysMenuMapper.findMenuByParentId(parentId);
        }
    }

    /**
     * 递归查询子菜单下的所有数据
     *
     * @param parentId
     * @return
     */
    protected List<MenuNode> findMenuNodeByParentId(Integer parentId) {
        List<MenuNode> data = new ArrayList<>();
        List<MenuNode> menu = sysMenuMapper.findMenuByParentId(parentId);
        if (ToolUtil.isNotEmpty(menu)) {
            data.addAll(menu);
            for (MenuNode sysMenu : menu) {
                List<MenuNode> childMenu = findMenuNodeByParentId(sysMenu.getId());
                if (ToolUtil.isNotEmpty(childMenu)) {
                    data.addAll(childMenu);
                }
            }
        }
        return data;
    }

    /**
     * 添加菜单
     *
     * @param sysMenu
     * @return
     */
    @Transactional
    @Override
    public boolean addOrUpdate(SysMenu sysMenu) {
        //获取当前登录人
        ShiroUser user = ShiroUtils.getUser();
        sysMenu.setCreateName(user.getName());
        sysMenu.setCreateTime(new Date());
        sysMenu.setCreateBy(user.getId());
        return super.insertOrUpdate(sysMenu);
    }


    /**
     * 删除按钮信息
     *
     * @param menuId
     * @return
     */
    @Transactional
    @Override
    public boolean deleteMenu(Integer menuId) {
        //查询按钮的子集
        List<MenuNode> menuNodes = this.findMenuNodeByParentId(menuId);
        if (ToolUtil.isNotEmpty(menuNodes)) {
            ArrayList<Integer> menuTree = menuNodes.stream().collect(() -> new ArrayList<Integer>(),
                    (list, menuNode) -> list.add(menuNode.getId()),
                    (list1, list2) -> list1.addAll(list2));
            sysMenuMapper.deleteBatchIds(menuTree);
        }
        return this.deleteById(menuId);
    }

}
