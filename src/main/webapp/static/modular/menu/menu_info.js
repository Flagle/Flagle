/**
 * 按钮信息
 * @author hrabbit
 * @Param:
 * @Date: 2019/1/12 1:40 PM
 */
var MenuInfo = {
    menuTableId: '',//table的id
    menuInfoData: null,
    menuTable: null, //MenuTable的对象
    layerIndex: -1
};


/**
 * 当点击顶级按钮
 */
MenuInfo.clickTopMenu = function (dom, id) {
    //添加选中class
    $("#topMenu div").removeClass("active");
    $(dom).addClass('active');
    //加载子集菜单
    MenuInfo.loadMenuList(id);
};

/**
 * 点击按钮
 */
MenuInfo.clickMenu = function (dom, id, pid) {
    //添加选中class
    $("#menuList div").removeClass("active");
    $(dom).addClass('active');
    //加载子集菜单
    MenuInfo.loadMenuInfo(id, pid);
};

/**
 * 加载顶级按钮
 */
MenuInfo.loadTopMenu = function () {
    var topMenuDoc = $('#topMenu');
    topMenuDoc.empty();
    var topMenu = "";
    //加载顶级菜单
    var ajax = new $ax("/menu/getMenuByParentId/-1", function (data) {
        for (var i = 0; i < data.length; i++) {
            if (i == 0) {
                topMenu += "<div class='list-group-item active' data-id='" + data[i].id + "' onclick='MenuInfo.clickTopMenu(this," + data[i].id + ")'>";
            } else {
                topMenu += "<div class='list-group-item' onclick='MenuInfo.clickTopMenu(this," + data[i].id + ")'>";
            }
            topMenu += "<div class='list-content'>";
            topMenu += "<i class='icon " + data[i].icon + "'></i>";
            topMenu += "<span class='top_menuname'> " + data[i].name + " </span>";
            topMenu += "<div class='item-actions'>";
            topMenu += "<span class='btn btn-pure btn-icon' onclick='MenuInfo.updateMenuToggle(" + data[i].id + ")'>";
            topMenu += "<i class='icon wb-edit' aria-hidden='true'></i></span>";
            topMenu += "<span class='btn btn-pure btn-icon' onclick='MenuInfo.delMenuToggle(" + data[i].id + ")'><i class='icon wb-close' aria-hidden='true'></i></span>";
            topMenu += "</div>";
            topMenu += "</div>";
            topMenu += "</div>";
        }
    }, function (data) {
    });
    ajax.setType("get");
    ajax.start();
    topMenuDoc.append(topMenu);
    var doms = $('#topMenu > .active');
    if (doms != null)
        MenuInfo.loadMenuList(doms.data('id'));
};


/**
 * 组装按钮html
 * @param MenuNode
 * @returns {string}
 */
MenuInfo.assembly = function (MenuNode) {
    var menu = "";
    var child = MenuNode.children;
    for (var k = 0; k < child.length; k++) {
        menu += "<ol class='dd-list'>";
        menu += "<li class='dd-item dd-item-alt'>";
        menu += "<div class='dd-handle'></div>";
        menu += "<div class='dd-content' data-id='" + child[k].id + "' data-pid='" + child[k].parentId + "' onclick='MenuInfo.clickMenu(this," + child[k].id + "," + child[k].parentId + ")'>";
        menu += "<span class='menu-name'><i class='icon " + child[k].icon + "'></i>" + child[k].name + "</span>";
        menu += "<span class='float-right fa-angle-right'></span>";
        menu += "</div>";
        if (child[k].children.length > 0) {
            var childMenu = this.assembly(child[k]);
            menu += childMenu;
        }
        menu += "</li>";
        menu += "</ol>";
    }
    return menu;
};


/**
 * 加载按钮列表
 */
MenuInfo.loadMenuList = function (parentId) {
    var menuDoc = $('#menuList');
    //清除menuDoc的html
    menuDoc.empty();
    var menu = "";
    //加载顶级菜单
    var ajax = new $ax("/menu/getMenuByParentId/" + parentId, function (data) {
        for (var i = 0; i < data.length; i++) {
            menu += "<ol class='dd-list animation-fade' id='menuTop'>";
            menu += "<li class='dd-item dd-item-alt menuIds'>";
            menu += "<div class='dd-handle'></div>";
            menu += "<div class='dd-content active' data-id='" + data[i].id + "' data-pid='" + data[i].parentId + "' onclick='MenuInfo.clickMenu(this," + data[i].id + "," + data[i].parentId + ")'>";
            menu += "<span class='menu-name'>" + data[i].name + "</span>";
            menu += "<span class='float-right fa-angle-right'></span></div>";
            if (data[i].children.length > 0) {
                var childMenu = MenuInfo.assembly(data[i]);
                menu += childMenu;
            }
            menu += "</li>";
            menu += "</ol>";
        }
    }, function (data) {
    });
    ajax.setType("get");
    ajax.start();
    menuDoc.append(menu);
    var dom = $('#menuTop').find('.active');
    if (dom != null && dom != undefined)
        MenuInfo.loadMenuInfo(dom.data('id'), dom.data('pid'));
};

MenuInfo.loadMenuInfo = function (id, parentId) {
    var menuInfo = "";
    var dom = $('#menuInfo');
    dom.empty();
    if (id == null) {
        menuInfo += "<h4>请输入菜单名称<button type='button' class='btn btn-pure btn-default btn-sm icon fa-trash-o float-right delete-submenu'></button></h4><hr>";
        menuInfo += "<div class='form fv-form fv-form-bootstrap'>";
        menuInfo += "<form id='compiler_submenu'>";
        menuInfo += "<div class='form-group'>";
        menuInfo += "<label class='col-form-label'>名称：</label>";
        menuInfo += "<input type='hidden' id='id' name='id'>";
        menuInfo += "<input type='hidden' id='parentId' value=" + parentId + " name='parentId'>";
        menuInfo += "<input type='text' class='form-control' name='title' id='title' placeholder='菜单名称' value='请输入菜单名称'>";
        menuInfo += "</div>";
        menuInfo += "<div class='form-group'>";
        menuInfo += "<label class='col-form-label'>菜单类型：</label>";
        menuInfo += "<select class='form-control' id='type'>";
        menuInfo += "<option value='menu'>菜单</option>";
        menuInfo += "<option value='button'>按钮</option>";
        menuInfo += "</select>";
        menuInfo += "</div>";
        menuInfo += "<div class='form-group'>";
        menuInfo += "<label class='col-form-label'>地址：</label>";
        menuInfo += "<input type='text' class='form-control' name='href' id='href' placeholder='菜单地址' value=''>";
        menuInfo += "</div>";
        menuInfo += "<div class='form-group'>";
        menuInfo += "<label class='col-form-label'>图标：</label>";
        menuInfo += "<input type='text' class='form-control icp-dd iconpicker-element' name='icon' id='icon' placeholder='菜单图标'>";
        menuInfo += "</div>";
        menuInfo += "</form></div>";

    } else {
        //加载顶级菜单
        var ajax = new $ax("/menu/getMenuById/" + id, function (data) {
            if (data != null) {
                menuInfo += "<h4>" + data.title + "<button type='button' class='btn btn-pure btn-default btn-sm icon fa-trash-o float-right delete-submenu'></button></h4><hr>";
                menuInfo += "<div class='form fv-form fv-form-bootstrap'>";
                menuInfo += "<form id='compiler_submenu'>";
                if (data.title != null && data.title != '') {
                    menuInfo += "<div class='form-group'>";
                    menuInfo += "<label class='col-form-label'>名称：</label>";
                    menuInfo += "<input type='hidden' id='id' value=" + id + " name='id'>";
                    menuInfo += "<input type='hidden' id='parentId' value=" + data.parentId + " name='parentId'>";
                    menuInfo += "<input type='text' class='form-control' name='title' id='title' placeholder='菜单名称' value='" + data.title + "'>";
                    menuInfo += "</div>";
                }
                if (data.type != null && data.type != '') {
                    menuInfo += "<div class='form-group'>";
                    menuInfo += "<label class='col-form-label'>菜单类型：</label>";
                    menuInfo += "<select class='form-control' id='type'>";
                    if (data.type == 'menu') {
                        menuInfo += "<option value='menu' selected>菜单</option>";
                        menuInfo += "<option value='button'>按钮</option>";
                    } else {
                        menuInfo += "<option value='menu'>菜单</option>";
                        menuInfo += "<option value='button' selected>按钮</option>";
                    }
                    menuInfo += "</select>";
                    menuInfo += "</div>";
                }
                if (data.href != null && data.href != '') {
                    menuInfo += "<div class='form-group'>";
                    menuInfo += "<label class='col-form-label'>地址：</label>";
                    menuInfo += "<input type='text' class='form-control' name='href' id='href' placeholder='菜单地址' value='" + data.href + "'>";
                    menuInfo += "</div>";
                }
                if (data.icon != null && data.icon != '') {
                    menuInfo += "<div class='form-group'>";
                    menuInfo += "<label class='col-form-label'>图标：</label>";
                    menuInfo += "<input type='text' class='form-control icp-dd iconpicker-element' name='icon' id='icon' placeholder='菜单图标' value='" + data.icon + "'>";
                    menuInfo += "</div>";
                }
                menuInfo += "</form></div>";
            }
        }, function (data) {
        });
        ajax.setType("get");
        ajax.start();
    }
    dom.append(menuInfo);
    //初始化图标选择器
    FlagleInitComponent.initIconpicker();
};


/**
 * 打开添加顶层按钮
 */
MenuInfo.addMenuToggle = function () {
    MenuInfo.layerIndex = HRABBIT.openPopup('添加菜单', '580', '540', '/menu/topMenuAdd');
};

/**
 * 打开修改顶层按钮
 */
MenuInfo.updateMenuToggle = function (id) {
    MenuInfo.layerIndex = HRABBIT.openPopup('修改菜单', '580', '540', '/menu/topMenuUpdate/' + id);
};

/**
 * 删除顶层按钮
 */
MenuInfo.delMenuToggle = function (id) {
    //发送ajax
    function operation() {
        //发送ajax
        var ajax = new $ax("/menu/delete/" + id, function (data) {
            HRABBIT.success("删除成功！");
            MenuInfo.loadTopMenu();
        }, function (data) {
            HRABBIT.error("删除失败!");
        });
        ajax.setType("DELETE");
        ajax.start();
    };
    //弹框提示
    HRABBIT.confirm("删除此菜单会自动删除子集菜单?", operation);
};


/**
 * 添加二级菜单
 * 0=>当前菜单之后
 * 1=>当前菜单里面
 */
MenuInfo.addMenu = function (type) {

    var doms = $('#menuTop').find('.active');

    if (doms != null && doms != undefined) {
        //先清空选中
        doms.removeClass('active');
        //获取父级id
        var pid = doms.data('pid');
        var id = doms.data('id');

        var menu = "";
        menu += "<ol class='dd-list'>";
        menu += "<li class='dd-item dd-item-alt'>";
        menu += "<div class='dd-handle'></div>";
        menu += "<div class='dd-content active' data-id='" + null + "' data-pid='" + id + "' onclick='MenuInfo.clickMenu(this," + null + "," + pid + ")'>";
        menu += "<span class='menu-name'><i class='icon fa-area-chart'></i>请输入菜单名称</span>";
        menu += "<span class='float-right fa-angle-right'></span>";
        menu += "</div>";
        menu += "</li>";
        menu += "</ol>";
        if (type == 0) {
            doms.parent().parent().after(menu);
        } else if (type == 1) {
            doms.after(menu);
        }
        MenuInfo.loadMenuInfo(null, id);
    }
};


/**
 * 清除数据
 */
MenuInfo.clearData = function () {
    this.menuInfoData = {};
};


/**
 * 收集数据
 */
MenuInfo.collectData = function () {
    this.set("id").set('title').set('parentId').set('href').set('icon').set('type')
};

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenuInfo.set = function (key, val) {
    this.menuInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
};


/**
 * 保存子菜单
 */
MenuInfo.saveChlidMenu = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax("/menu/add", function (data) {
        HRABBIT.success("保存成功！");
        MenuInfo.loadTopMenu();
    }, function (data) {
        HRABBIT.error('保存失败！');
    });
    ajax.set(MenuInfo.menuInfoData);
    ajax.start();
};

/**
 * 初始化
 * @author hrabbit
 * @Param:
 * @Date: 2019/1/12 1:41 PM
 */
$(function () {
    //加载顶级Menu
    MenuInfo.loadTopMenu();
});