/**
 * 按钮信息
 * @author hrabbit
 * @Param:
 * @Date: 2019/1/12 1:40 PM
 */
var MenuTopAdd = {
    menuInfoData: null,
    //表单规则
    validateFields: {
        title: {
            validators: {
                notEmpty: {
                    message: '菜单不能为空'
                }
            }
        },
        code: {
            validators: {
                notEmpty: {
                    message: '菜单代码不能为空'
                }
            }
        },
        icon: {
            validators: {
                notEmpty: {
                    message: '菜单图标不能为空'
                }
            }
        },
        selectDef: {
            validators: {
                notEmpty: {
                    message: '所属部门不能为空'
                }
            }
        }
    }
};

/**
 * 表单验证
 */
MenuTopAdd.validate = function () {
    $('#controlMenu').data("bootstrapValidator").resetForm();
    $('#controlMenu').bootstrapValidator('validate');
    return $("#controlMenu").data('bootstrapValidator').isValid();
};

/**
 * 清除数据
 */
MenuTopAdd.clearData = function () {
    this.menuInfoData = {};
};


/**
 * 收集数据
 */
MenuTopAdd.collectData = function () {
    this.set("id").set('title').set('parentId').set('code').set('icon').set('type').set('selectDef')
};

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenuTopAdd.set = function (key, val) {
    this.menuInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
};

/**
 * 添加用户角色
 */
MenuTopAdd.submitAddMenu = function () {
    this.clearData();
    this.collectData();
    //验证表单
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax("/menu/add", function (data) {
        parent.HRABBIT.success("添加成功！");
        window.parent.MenuInfo.loadTopMenu();
        MenuTopAdd.close();
    }, function (data) {
        parent.HRABBIT.error('添加失败！');
    });
    ajax.set(MenuTopAdd.menuInfoData);
    ajax.start();
};

/**
 * 修改用户角色
 */
MenuTopAdd.submitUpdateMenu = function () {
    this.clearData();
    this.collectData();
    //验证表单
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax("/menu/update", function (data) {
        parent.HRABBIT.success("修改成功！");
        window.parent.MenuInfo.loadTopMenu();
        MenuTopAdd.close();
    }, function (data) {
        parent.HRABBIT.error('修改失败！');
    });
    ajax.set(MenuTopAdd.menuInfoData);
    ajax.start();
};

/**
 * 关闭此对话框
 */
MenuTopAdd.close = function () {
    parent.layer.close(window.parent.MenuInfo.layerIndex);
};

$(function () {
    //开启表单验证
    HRABBIT.initValidator('controlMenu', MenuTopAdd.validateFields);
});